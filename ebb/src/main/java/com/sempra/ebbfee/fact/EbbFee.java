package com.sempra.ebbfee.fact;

import java.io.Serializable;

public class EbbFee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3212603988560840121L;

	public double ebbFeeAmount;

	public EbbFee() {
		super();
	}

	public double getEbbFeeAmount() {
		return ebbFeeAmount;
	}

	public void setEbbFeeAmount(double ebbFeeAmount) {
		this.ebbFeeAmount = ebbFeeAmount;
	}

}
