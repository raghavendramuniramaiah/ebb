package com.sempra.ebbfee.fact;

import java.io.Serializable;

public class AccountInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -257101363734086802L;

	public String accountType = "";
	public boolean isMarketer;
	public boolean isStorageBroker;

	public AccountInfo() {
		super();
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public boolean isMarketer() {
		return isMarketer;
	}

	public void setMarketer(boolean isMarketer) {
		this.isMarketer = isMarketer;
	}

	public boolean isStorageBroker() {
		return isStorageBroker;
	}

	public void setStorageBroker(boolean isStorageBroker) {
		this.isStorageBroker = isStorageBroker;
	}

}
