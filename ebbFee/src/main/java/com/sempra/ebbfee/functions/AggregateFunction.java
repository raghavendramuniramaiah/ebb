package com.sempra.ebbfee.functions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class AggregateFunction {

	public static Double SUM(List<Double> objects) {
		
		
		Double d = new Double(0);
		for (Double object : objects) {
			Double dObj = new Double(object);
			d += dObj;
		}
		return d;
	}

	@SuppressWarnings("unchecked")
	public static Double SUM(Object... objects) {
		Double d = new Double(0);
		for (Object object : objects) {
			if(object instanceof List){
				d+=SUM((List<Object>)object);
			}else{
				d+=(Double)object;
			}
		}
		return d;
	}
	
	public static Double ROUND(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
}
