package com.sempra.ebbfee.fact;

import java.io.Serializable;

public class HoldingBillingChargeSummary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4411401357518334422L;

	public HoldingBillingChargeSummary() {
		super();
	}

	public double gasSelectCharge;
	public double customerCharge;
	public double reservationCharge;

	public double getGasSelectCharge() {
		return gasSelectCharge;
	}

	public void setGasSelectCharge(double gasSelectCharge) {
		this.gasSelectCharge = gasSelectCharge;
	}

	public double getCustomerCharge() {
		return customerCharge;
	}

	public void setCustomerCharge(double customerCharge) {
		this.customerCharge = customerCharge;
	}

	public double getReservationCharge() {
		return reservationCharge;
	}

	public void setReservationCharge(double reservationCharge) {
		this.reservationCharge = reservationCharge;
	}

}
