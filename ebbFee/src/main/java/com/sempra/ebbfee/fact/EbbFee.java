package com.sempra.ebbfee.fact;

import java.io.Serializable;

public class EbbFee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3212603988560840121L;

	public double ebbFeeAmount;
	
	public double shortTermBaseFee;
	public double shortTermUsage;
	public double longTermBaseFee;
	public double longTermUsage;
	

	public EbbFee() {
		super();
	}

	public double getEbbFeeAmount() {
		return ebbFeeAmount;
	}

	public void setEbbFeeAmount(double ebbFeeAmount) {
		this.ebbFeeAmount = ebbFeeAmount;
	}

	public double getShortTermBaseFee() {
		return shortTermBaseFee;
	}

	public void setShortTermBaseFee(double shortTermBaseFee) {
		this.shortTermBaseFee = shortTermBaseFee;
	}

	public double getShortTermUsage() {
		return shortTermUsage;
	}

	public void setShortTermUsage(double shortTermUsage) {
		this.shortTermUsage = shortTermUsage;
	}

	public double getLongTermBaseFee() {
		return longTermBaseFee;
	}

	public void setLongTermBaseFee(double longTermBaseFee) {
		this.longTermBaseFee = longTermBaseFee;
	}

	public double getLongTermUsage() {
		return longTermUsage;
	}

	public void setLongTermUsage(double longTermUsage) {
		this.longTermUsage = longTermUsage;
	}

	
	
}
