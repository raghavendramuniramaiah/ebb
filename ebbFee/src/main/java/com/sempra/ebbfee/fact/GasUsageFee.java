package com.sempra.ebbfee.fact;

import java.io.Serializable;
import java.util.List;

public class GasUsageFee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2696369592199372690L;

	public GasUsageFee() {
		super();
	}

	public double shortTermBaseFee;
	public double shortTermUsage;
	public double shortTermUsageFeeRate;
	public double longTermBaseFee;
	public double longTermUsageFeeRate;
	public List<Double> subscriberUsage;

	public double getShortTermBaseFee() {
		return shortTermBaseFee;
	}

	public void setShortTermBaseFee(double shortTermBaseFee) {
		this.shortTermBaseFee = shortTermBaseFee;
	}

	public double getShortTermUsage() {
		return shortTermUsage;
	}

	public void setShortTermUsage(double shortTermUsage) {
		this.shortTermUsage = shortTermUsage;
	}

	public double getShortTermUsageFeeRate() {
		return shortTermUsageFeeRate;
	}

	public void setShortTermUsageFeeRate(double shortTermUsageFeeRate) {
		this.shortTermUsageFeeRate = shortTermUsageFeeRate;
	}

	public double getLongTermBaseFee() {
		return longTermBaseFee;
	}

	public void setLongTermBaseFee(double longTermBaseFee) {
		this.longTermBaseFee = longTermBaseFee;
	}

	public double getLongTermUsageFeeRate() {
		return longTermUsageFeeRate;
	}

	public void setLongTermUsageFeeRate(double longTermUsageFeeRate) {
		this.longTermUsageFeeRate = longTermUsageFeeRate;
	}

	public List<Double> getSubscriberUsage() {
		return subscriberUsage;
	}

	public void setSubscriberUsage(List<Double> subscriberUsage) {
		this.subscriberUsage = subscriberUsage;
	}

}
